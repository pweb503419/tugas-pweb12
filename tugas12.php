<?php
    // Array berisi nilai-nilai mahasiswa
    $nilai_mahasiswa = array(85, 78, 92, 74, 88, 90, 79);

    // Fungsi untuk menghitung rata-rata nilai
    function hitung_rata_rata($nilai) {
        $total = 0;
        $jumlah_nilai = count($nilai);
        
        foreach ($nilai as $n) {
            $total += $n;
        }
        
        return $total / $jumlah_nilai;
    }

    // Hitung rata-rata nilai
    $rata_rata = hitung_rata_rata($nilai_mahasiswa);

    // Tampilkan rata-rata nilai
    echo "Rata-rata Nilai Mahasiswa";
    echo "<p>Nilai: " . implode(", ", $nilai_mahasiswa) . "</p>";
    echo "<p>Rata-rata: " . number_format($rata_rata, 2) . "</p>";
    ?>
